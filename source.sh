# This is the source.sh script. It is executed by BPM in a temporary directory when compiling a source package
# BPM Expects the source code to be extracted into the automatically created 'source' directory which can be accessed using $BPM_SOURCE
# BPM Expects the output files to be present in the automatically created 'output' directory which can be accessed using $BPM_OUTPUT

DOWNLOAD="https://github.com/shadow-maint/shadow/releases/download/${BPM_PKG_VERSION}/shadow-${BPM_PKG_VERSION}.tar.xz"
FILENAME="${DOWNLOAD##*/}"

# The prepare function is executed in the root of the temp directory
# This function is used for downloading files and putting them into the correct location
prepare() {
  wget "$DOWNLOAD"
  tar -xvf "$FILENAME" --strip-components=1 -C "$BPM_SOURCE"
}

# The build function is executed in the source directory
# This function is used to compile the source code
build() {
  sed -i 's/groups$(EXEEXT) //' src/Makefile.in
  find man -name Makefile.in -exec sed -i 's/groups\.1 / /'   {} \;
  find man -name Makefile.in -exec sed -i 's/getspnam\.3 / /' {} \;
  find man -name Makefile.in -exec sed -i 's/passwd\.5 / /'   {} \;
  
  sed -e 's:#ENCRYPT_METHOD DES:ENCRYPT_METHOD YESCRYPT:' \
      -e 's:/var/spool/mail:/var/mail:'                   \
      -e '/PATH=/{s@/sbin:@@;s@/bin:@@}'                  \
      -i etc/login.defs
  mkdir -p "$BPM_OUTPUT"/usr/bin
  touch "$BPM_OUTPUT"/usr/bin/passwd
  ./configure --sysconfdir=/etc   \
              --disable-static    \
              --with-{b,yes}crypt \
              --without-libbsd    \
              --with-group-name-max-length=32
  make
}

# The package function is executed in the source directory
# This function is used to move the compiled files into the output directory
package() {
  make DESTDIR="$BPM_OUTPUT" exec_prefix=/usr pamdir= install
  make -C man DESTDIR="$BPM_OUTPUT" install-man
  mkdir -p "$BPM_OUTPUT"/etc/pam.d
  for file in ../pam.d/*; do
    cp "$file" "$BPM_OUTPUT"/etc/pam.d
  done
  cp ../login.defs "$BPM_OUTPUT"/etc/login.defs
  mkdir -p "$BPM_OUTPUT"/usr/share/licenses/shadow
  cp COPYING "$BPM_OUTPUT"/usr/share/licenses/shadow/COPYING
}
